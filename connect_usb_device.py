import usb.core
import usb.util
import argparse


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--id_product", required=True)
    parser.add_argument("--id_vendor", required=True)
    args = parser.parse_args()
    return args


def _usb(id_product,id_vendor):
# find our device
    dev = usb.core.find(idVendor=id_vendor, idProduct=id_product)

# was it found?
    if dev is None:
        raise ValueError('Device not found')
    print(f"////device////\n{dev} \n\n")
# set the active configuration. With no arguments, the first
# configuration will be the active one
    dev.set_configuration()

# get an endpoint instance
    cfg = dev.get_active_configuration()
    intf = cfg[(1,0)]
    ep = usb.util.find_descriptor(
            intf,
            # match the first OUT endpoint
            custom_match = \
            lambda e: \
                usb.util.endpoint_direction(e.bEndpointAddress)== \
                usb.util.ENDPOINT_OUT)

    assert ep is not None
    #ep.write("hello world")
    print("adress choose", ep.bEndpointAddress)
    print(ep.write(1))

if __name__ == "__main__":
    args = get_args()
    _usb(int(args.id_product,16), int(args.id_vendor,16))
