# !/usr/bin/env python
"""Play a fixed frequency sound."""
from __future__ import division
import math
from pyaudio import PyAudio
import numpy as np
import matplotlib.pyplot as plt
import click
import time


@click.group('audio')
def cli():
    pass


def plot(x, y):
    plt.plot(x, y)
    plt.show()


def np_sine(frequence, duration, volume, f_echant):
    N = f_echant * duration
    times = np.linspace(0, duration, int(N))
    sinus = volume * np.sin(2 * np.pi * frequence * times) + 128

    return bytes(bytearray(sinus.astype(int).tolist()))


def math_sine(f, d, v, Fs):
    N = Fs * d
    times = np.linspace(0, d, int(N))
    s = lambda t: v * math.sin(2 * math.pi * f * t)
    samples = [int(s(t) + 128) for t in times]
    return bytes(bytearray(samples))


def sine_tone(frequency, duration, volume=10, sample_rate=44000):
    n_samples = int(sample_rate * duration)
    restframes = n_samples % sample_rate
    p = PyAudio()
    stream = p.open(format=p.get_format_from_width(1),  # 8bit
                    channels=1,  # mono
                    rate=sample_rate,
                    output=True)
    # s = lambda t: volume * math.sin(2 * math.pi * frequency * t / sample_rate)
    # samples = (int(s(t) + 128) for t in range(n_samples))
    # for buf in zip(*[samples] * sample_rate):  # write several samples at a time
    #     stream.write(bytes(buf))

    stream.write(math_sine(frequency, duration, volume, sample_rate))
    stream.write(np_sine(frequency, duration, volume, sample_rate))

    # fill remainder of frameset with silence
    stream.write(b'\x80' * restframes)

    stream.stop_stream()
    stream.close()
    p.terminate()


@cli.command()
@click.option('--tempo', required=True)
@click.option('-d', required=True, help="duration of the play in minutes")
@click.option('--bar', required=True, help="bar of the play")
def metronome(tempo, d, bar):
    """
    :param tempo: bpm hit per minute
    :return:
    """
    FREQ_SAMPLE = 44100
    p = PyAudio()

    count = 0

    stream = p.open(format=p.get_format_from_width(2),  # 8bit
                    channels=2,  # mono
                    rate=FREQ_SAMPLE,
                    output=True)

    start_time = time.time()
    now_time = time.time()

    while now_time - start_time < float(d)*60:

        print(count)

        if count == int(bar):
            stream.write(np_sine(560, 0.1, 4, FREQ_SAMPLE))
            count = 0
        else:
            stream.write(np_sine(440, 0.1, 4, FREQ_SAMPLE))

        time.sleep(60 * 1000 / int(tempo) / 1000)
        count += 1
        now_time = time.time()

    stream.stop_stream()
    stream.close()
    p.terminate()


if __name__ == "__main__":
    cli()
